package com.example.demo12.Service;

import com.example.demo12.Entity.TinhEntity;
import com.example.demo12.Entity.TinhEntity;
import com.example.demo12.Entity.TinhEntity;
import com.example.demo12.Model.Tinh;
import com.example.demo12.Model.Tinh;
import com.example.demo12.Model.Tinh;
import com.example.demo12.Repository.LocationRepository;
import com.example.demo12.Repository.TinhRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TinhServiceImp implements TinhService {
    @Autowired
    TinhRepository tinhRepository;
    @Autowired
    LocationRepository locationRepository;
    @Override
    public List<Tinh> getAllTinh() {
        List<TinhEntity> tinhEntities=tinhRepository.findAll();
        List<Tinh> tinhs=new ArrayList<>();
        for(TinhEntity tinhEntity:tinhEntities){
            Tinh tinh=parseEntityToModel(tinhEntity);
            tinhs.add(tinh);
        }
        return tinhs;
    }

    @Override
    public Tinh getTinhByID(Integer id) {
        TinhEntity tinhEntity=tinhRepository.findTinhEntityById(id);
        Tinh tinh=parseEntityToModel(tinhEntity);
        return tinh;
    }

    @Override
    public Tinh parseEntityToModel(TinhEntity tinhEntity) {
        Tinh tinh=new Tinh();
        tinh.setId(tinhEntity.getid());
        tinh.setTenTinh(tinhEntity.getTenTinh());
        return tinh;
    }

    @Override
    public TinhEntity parseModelToEntity(Tinh tinh) {
        TinhEntity tinhEntity=new TinhEntity();
        if(Objects.nonNull(tinh.getId())){
            tinhEntity.setid(tinh.getId());
        }
        tinhEntity.setTenTinh(tinh.getTenTinh());
        return tinhEntity;
    }

    @Override
    public int createTinh(Tinh tinh) {
        TinhEntity tinhEntity=parseModelToEntity(tinh);
        TinhEntity tinhEntity1=tinhRepository.save(tinhEntity);
        return tinhEntity1.getid();
    }

    @Override
    public void updateTinh(Tinh tinh) {
        TinhEntity tinhEntity=parseModelToEntity(tinh);
        tinhRepository.save(tinhEntity);

    }

    @Override
    public void deleteTinh(Integer id) {
        TinhEntity TinhEntity=tinhRepository.findTinhEntityById(id);
        tinhRepository.delete(TinhEntity);
    }
}
