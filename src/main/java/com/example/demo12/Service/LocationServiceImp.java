package com.example.demo12.Service;

import com.example.demo12.Entity.LocationEntity;
import com.example.demo12.Model.Location;
import com.example.demo12.Repository.LocationRepository;
import com.example.demo12.Repository.TinhRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class LocationServiceImp implements LocationService {
    @Autowired
    LocationRepository locationRepository;
    @Autowired
    TinhRepository tinhRepository;
    @Override
    public List<Location> getAllLocation() {
        List<LocationEntity> locationEntities=locationRepository.findAll();
        List<Location> locations=new ArrayList<>();
        for(LocationEntity locationEntity:locationEntities){
            Location location=parseEntityToModel(locationEntity);
            locations.add(location);
        }
        return locations;
    }

    @Override
    public Location getLocationByID(Integer id) {
        LocationEntity locationEntity=locationRepository.findLocationEntityById(id);
        Location location=parseEntityToModel(locationEntity);
        return location;
    }

    @Override
    public Location parseEntityToModel(LocationEntity locationEntity) {
        Location location=new Location();
        location.setId(locationEntity.getId());
        location.setLat(locationEntity.getLat());
        location.setLong(locationEntity.getLon());
        location.setName(locationEntity.getName());
        return location;
    }

    @Override
    public LocationEntity parseModelToEntity(Location location) {
        LocationEntity locationEntity=new LocationEntity();
        if(Objects.nonNull(location.getId())){
            locationEntity.setId(location.getId());
        }
        locationEntity.setLat(location.getLat());
        locationEntity.setLon(location.getLong());
        locationEntity.setName(location.getName());
        locationEntity.setTinhEntity(tinhRepository.findTinhEntityById(location.getTinhId()));
        return locationEntity;
    }

    @Override
    public int createLocation(Location location) {
        LocationEntity locationEntity=parseModelToEntity(location);
        LocationEntity locationEntity1=locationRepository.save(locationEntity);
        return locationEntity1.getId();
    }

    @Override
    public void updateLocation(Location location) {
        LocationEntity locationEntity=parseModelToEntity(location);
        locationRepository.save(locationEntity);

    }

    @Override
    public void deleteLocation(Integer id) {
        LocationEntity locationEntity=locationRepository.findLocationEntityById(id);
        locationRepository.delete(locationEntity);
    }
}
