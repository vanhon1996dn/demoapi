package com.example.demo12.Service;

import com.example.demo12.Entity.LocationEntity;
import com.example.demo12.Model.Location;

import java.util.List;

public interface LocationService {
    public List<Location> getAllLocation();
    public Location getLocationByID(Integer id);
    public Location parseEntityToModel(LocationEntity locationEntity);
    public LocationEntity parseModelToEntity(Location location);
    public int createLocation(Location location);
    public void updateLocation(Location location);
    public void deleteLocation(Integer id);
}
