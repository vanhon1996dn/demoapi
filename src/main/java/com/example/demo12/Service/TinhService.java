package com.example.demo12.Service;

import com.example.demo12.Entity.TinhEntity;
import com.example.demo12.Model.Tinh;

import java.util.List;

public interface TinhService {
    public List<Tinh> getAllTinh();
    public Tinh getTinhByID(Integer id);
    public Tinh parseEntityToModel(TinhEntity tinhEntity);
    public TinhEntity parseModelToEntity(Tinh tinh);
    public int createTinh(Tinh tinh);
    public void updateTinh(Tinh tinh);
    public void deleteTinh(Integer id);
}
