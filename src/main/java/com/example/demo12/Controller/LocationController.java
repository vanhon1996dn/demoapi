package com.example.demo12.Controller;

import com.example.demo12.Model.Location;
import com.example.demo12.Service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Table;
import javax.validation.Valid;
import java.util.List;

@RestController()
public class LocationController {
    @Autowired
    private LocationService locationService;
    @GetMapping("/Location")
    private ResponseEntity getAllLocation(){
        List<Location> locationList=locationService.getAllLocation();
        return new ResponseEntity(locationList,HttpStatus.CREATED);

    }
    @GetMapping("/Location/{id}")
    private ResponseEntity getLocation(@PathVariable(value = "id") int locationID){
        Location location=locationService.getLocationByID(locationID);
        return new ResponseEntity(location,HttpStatus.CREATED);
    }

    @PostMapping("/Location/create")
    public ResponseEntity createLocation(@RequestBody Location location){

        int locationID = locationService.createLocation(location);
        return new ResponseEntity(locationID ,HttpStatus.CREATED);
    }
    @PutMapping("/Location/update")
    public ResponseEntity updateLocation(@RequestBody  Location location) {
        locationService.updateLocation(location);
        return new ResponseEntity(HttpStatus.OK);
    }
    @DeleteMapping("/Location/delete")
    public ResponseEntity deleteLocation(@RequestBody  Location location) {
        locationService.deleteLocation(location.getId());
        return new ResponseEntity(HttpStatus.OK);
    }
}
