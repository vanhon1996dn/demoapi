package com.example.demo12.Controller;

import com.example.demo12.Model.Location;
import com.example.demo12.Model.Tinh;
import com.example.demo12.Service.LocationService;
import com.example.demo12.Service.TinhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/")
public class TinhController {
    @Autowired
    private TinhService tinhService;
    @GetMapping("/Tinh")
    private ResponseEntity getAllTinh(){
        List<Tinh> TinhList=tinhService.getAllTinh();
        return new ResponseEntity(TinhList, HttpStatus.CREATED);

    }
    @GetMapping("/Tinh/{id}")
    private ResponseEntity getTinh(@PathVariable(value = "id") int tinhID){
        Tinh tinh=tinhService.getTinhByID(tinhID);
        return new ResponseEntity(tinh,HttpStatus.CREATED);
    }

    @PostMapping("/tinh/create")
    public ResponseEntity createTinh(@RequestBody Tinh tinh){

        int tinhID = tinhService.createTinh(tinh);
        return new ResponseEntity(tinhID ,HttpStatus.CREATED);
    }
    @PutMapping("/tinh/update")
    public ResponseEntity updateTinh(@RequestBody  Tinh tinh) {
        tinhService.updateTinh(tinh);
        return new ResponseEntity(HttpStatus.OK);
    }
    @DeleteMapping("/tinh/delete")
    public ResponseEntity deleteTinh(@RequestBody  Tinh tinh) {
        tinhService.deleteTinh(tinh.getId());
        return new ResponseEntity(HttpStatus.OK);
    }

}
