package com.example.demo12.Entity;



import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name ="location")
public class LocationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double lat;
    private Double lon;
    private String name;
    @ManyToOne
    @JoinColumn(name="tinhId")
    private TinhEntity tinhEntity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TinhEntity getTinhEntity() {
        return tinhEntity;
    }

    public void setTinhEntity(TinhEntity tinhEntity) {
        this.tinhEntity = tinhEntity;
    }
}


