package com.example.demo12.Model;

public class Location {
    private Integer Id;
    private Double Lat;
    private Double Long;
    private String Name;
    private Integer TinhId;

    public Integer getTinhId() {
        return TinhId;
    }

    public void setTinhId(Integer tinhId) {
        TinhId = tinhId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }

    public Double getLong() {
        return Long;
    }

    public void setLong(Double aLong) {
        Long = aLong;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
