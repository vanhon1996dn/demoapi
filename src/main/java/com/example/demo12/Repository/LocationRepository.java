package com.example.demo12.Repository;

import com.example.demo12.Entity.LocationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends CrudRepository<LocationEntity,Integer> {
    List<LocationEntity> findAll();
    LocationEntity findLocationEntityById(Integer ID);
}
