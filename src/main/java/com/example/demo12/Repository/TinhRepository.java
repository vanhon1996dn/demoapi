package com.example.demo12.Repository;

import com.example.demo12.Entity.TinhEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TinhRepository extends CrudRepository<TinhEntity,Integer> {
    List<TinhEntity> findAll();
    TinhEntity findTinhEntityById(Integer id);
}
